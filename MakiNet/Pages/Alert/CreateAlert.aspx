﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateAlert.aspx.cs" Inherits="MakiNet.Views.Alert.CreateAlert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-size: small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Create Alert</h1>
            GUID:&nbsp;
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            <br />
            <span class="auto-style1"><em>Record this key and keep it in a safe place! You will need it if you ever want to update or remove the alert!</em></span><br />
            Operator Key:
            <asp:TextBox ID="TextBox4" runat="server" Width="297px"></asp:TextBox>
            <br />
            <br />
            Alert Type: <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>
            <br />
            Alert Location:
            <asp:DropDownList ID="DropDownList2" runat="server">
            </asp:DropDownList>
            <br />
            Start Time:
            <asp:TextBox ID="TextBox1" runat="server" TextMode="DateTimeLocal"></asp:TextBox>
            <br />
            End Time:
            <asp:TextBox ID="TextBox2" runat="server" style="margin-bottom: 0px" TextMode="DateTimeLocal"></asp:TextBox>
            <br />
            <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" Text="Is Infinite" />
            <br />
            Message:<br />
            <asp:TextBox ID="TextBox3" runat="server" Height="138px" TextMode="MultiLine" Width="303px"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit" />
        </div>
        <br />
        Response:<br />
        <asp:TextBox ID="TextBox5" runat="server" Height="140px" ReadOnly="True" TextMode="MultiLine" Width="304px"></asp:TextBox>
    </form>
</body>
</html>
