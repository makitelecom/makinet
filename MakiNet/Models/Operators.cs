﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MakiNet.Models
{
    public class Operators
    {
        /*
         * WARNING
         *
         * Operator keys are publicly visible. Change before deploying to server!
         *
         */
        private static string[] ids = new string[]{
            "02206EDE-04FC-47AF-879B-51D1CEDB67A4"
        };

        public static bool isOK(Guid op)
        {
            return ids.Contains(op.ToString().ToUpper());
        }
}
}