﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MakiNet.Models
{
    public class AlertDataResponse
    {
        public bool foundAlert { get; set; }
        public List<Alert> alerts { get; set; }
    }
}