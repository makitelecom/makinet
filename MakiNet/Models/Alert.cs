﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MakiNet.Models
{
    public class Alert
    {
        public Resources.AlertTypes myType;
        public Resources.LocationShortCode myLoc;
        public DateTime startTime;
        public DateTime endTime;
        public string message;
        public Guid myID;
        public bool isIndefinite;

        public Alert()
        {
            myID = Guid.NewGuid();
        }
    }
}