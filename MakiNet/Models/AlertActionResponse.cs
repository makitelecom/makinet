﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MakiNet.Models
{
    public class AlertActionResponse
    {

        public enum StatusCode
        {
            OK,
            UNAUTHORIZED,
            GUID_EXISTS,
            GUID_NOT_FOUND
        }
        public bool wasActionSucessful { get; set; }
        public StatusCode statusCode { get; set; }
        public string message { get; set; }
    }
}