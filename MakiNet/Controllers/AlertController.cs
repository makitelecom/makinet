﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Antlr.Runtime.Tree;
using MakiNet.Models;
using Newtonsoft.Json;
using Action = MakiNet.Models.Action;

namespace MakiNet.Controllers
{
    public class AlertController : Controller
    {
        private const bool isTestMode = false;
        private static activeAlertsEntities1 _db = new activeAlertsEntities1();

        private static DateTime lastPurge = DateTime.MinValue;
        // GET: Alert
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewRaw()
        {
            return View(_db.AlertEntries.ToList());
        }

        public ActionResult ViewAll()
        {
            if (isTestMode)
            {
                return ViewAllTest();
            }
            List<Alert> foundAlerts = new List<Alert>();
            foreach (var i in _db.AlertEntries)
            {
                if (i.endtime > DateTime.Now)
                {
                    foundAlerts.Add(fromDatabase(getAlert(Guid.Parse(i.guid))));
                }
            }
            return View(foundAlerts);
        }

        private ActionResult ViewAllTest()
        {
            List<Alert> foundAlerts = new List<Alert>();
            Alert test = new Alert();
            test.myType = Resources.AlertTypes.RMT;
            test.myLoc = Resources.LocationShortCode.ALL;
            test.message = "This is a test of the MakiNet system. This is only a test. ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            test.startTime = DateTime.Now;
            test.endTime = DateTime.Now.AddMinutes(1);
            test.isIndefinite = false;
            foundAlerts.Add(test);
            Alert test2 = new Alert();
            test2.myType = Resources.AlertTypes.HTA;
            test2.myLoc = Resources.LocationShortCode.SM;
            test2.message = "The Makislavian Weather Service in Bellevue has issued a Heat Advisory...which is in effect until 8 AM MST Monday until 8 PM MST Thursday.\n * TEMPERATURES...Highs of 80 to 85.\n * TIMING...Through this evening.\n * IMPACTS...Hot temperatures will increase chances for heat related illness especially for those without access to air conditioning. Heat stress is also possible for livestock and outdoor pets.";
            test2.startTime = new DateTime(2018,8,13,08,00,00);
            test2.endTime = new DateTime(2018,8,15,16,00,00);
            test2.isIndefinite = false;
            foundAlerts.Add(test2);
            Alert test3 = new Alert();
            test3.myType = Resources.AlertTypes.RFW;
            test3.myLoc = Resources.LocationShortCode.EM;
            test3.message = "Hot, dry and unstable conditions will persist into this afternoon. Dry and breezy winds increase late Friday afternoon and evening and will continue into Saturday. Isolated dry thunderstorms are expected overnight.";
            test3.startTime = new DateTime(2018, 8, 10, 13, 00, 00);
            test3.endTime = DateTime.MaxValue;
            test3.isIndefinite = true;
            foundAlerts.Add(test3);
            return View(foundAlerts);
        }

        public ActionResult ViewZip(int zip)
        {
            if (!Resources.LocationZipMapping.ContainsKey(zip))
            {
                throw new Exception("ZIP is not in service area.");
            }
            List<Resources.LocationShortCode> validLoc = Resources.LocationZipMapping[zip];
            List<Alert> foundAlerts = new List<Alert>();
            foreach (var i in _db.AlertEntries)
            {
                if (validLoc.Contains((Resources.LocationShortCode)i.location) && i.endtime > DateTime.Now)
                {
                    foundAlerts.Add(fromDatabase(getAlert(Guid.Parse(i.guid))));
                }
            }
            return View(foundAlerts);
        }

        public ActionResult ViewLoc(int loc)
        {
            List<Alert> foundAlerts = new List<Alert>();
            foreach (var i in _db.AlertEntries)
            {
                if (i.location == loc && i.endtime > DateTime.Now)
                {
                    foundAlerts.Add(fromDatabase(getAlert(Guid.Parse(i.guid))));
                }
            }
            return View(foundAlerts);
        }

        public RedirectResult CreateAlert()
        {
            return Redirect("~/Pages/Alert/CreateAlert.aspx");
        }

        [HttpPost]
        public ActionResult PerformAction()
        {
            AlertActionResponse response = new AlertActionResponse();
            Request.InputStream.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(Request.InputStream).ReadToEnd();
            AlertActionRequest request = JsonConvert.DeserializeObject<AlertActionRequest>(json);
            if (!Operators.isOK(request.key))
            {
                response.message = "The provided operator key was invalid.";
                response.wasActionSucessful = false;
                response.statusCode = AlertActionResponse.StatusCode.UNAUTHORIZED;
                return Content(JsonConvert.SerializeObject(response));
            }
            else
            {
                AlertEntry newEntry;
                switch (request.action)
                {
                        
                    case Action.Add:
                        if (hasAlert(request.alert.myID))
                        {
                            response.message = "An alert with the GUID already exists.";
                            response.wasActionSucessful = false;
                            response.statusCode = AlertActionResponse.StatusCode.GUID_EXISTS;
                            return Content(JsonConvert.SerializeObject(response));
                        }
                        newEntry = new AlertEntry();
                        newEntry.guid = request.alert.myID.ToString();
                        newEntry.type = (int)request.alert.myType;
                        newEntry.location = (int) request.alert.myLoc;
                        newEntry.starttime = request.alert.startTime;
                        newEntry.endtime = request.alert.endTime;
                        newEntry.indefinite = request.alert.isIndefinite;
                        newEntry.message = request.alert.message;
                        _db.AlertEntries.Add(newEntry);
                        _db.SaveChanges();
                        break;
                    case Action.Update:
                        
                        AlertEntry oldEntry = getAlert(request.alert.myID);
                        newEntry = new AlertEntry();
                        if (oldEntry == null)
                        {
                            response.message = "The alert could not be found.";
                            response.wasActionSucessful = false;
                            response.statusCode = AlertActionResponse.StatusCode.GUID_NOT_FOUND;
                            return Content(JsonConvert.SerializeObject(response));
                        }
                        else
                        {
                            newEntry.guid = request.alert.myID.ToString();
                            newEntry.type = (int)request.alert.myType;
                            newEntry.location = (int)request.alert.myLoc;
                            newEntry.starttime = request.alert.startTime;
                            newEntry.endtime = request.alert.endTime;
                            newEntry.indefinite = request.alert.isIndefinite;
                            newEntry.message = request.alert.message;
                            _db.AlertEntries.Remove(oldEntry);
                            _db.AlertEntries.Add(newEntry);
                            _db.SaveChanges();
                        }
                        break;
                    case Action.Remove:
                        newEntry = getAlert(request.alert.myID);
                        if (newEntry == null)
                        {
                            response.message = "The alert could not be found.";
                            response.wasActionSucessful = false;
                            response.statusCode = AlertActionResponse.StatusCode.GUID_NOT_FOUND;
                            return Content(JsonConvert.SerializeObject(response));
                        }
                        else
                        {
                            _db.AlertEntries.Remove(newEntry);
                            _db.SaveChanges();
                        }
                        break;
                }
                response.message = "The operation was completed.";
                response.wasActionSucessful = true;
                response.statusCode = AlertActionResponse.StatusCode.OK;
                return Content(JsonConvert.SerializeObject(response));
            }
        }

        public ActionResult purge()
        {
            if (lastPurge == null)
            {
                lastPurge = DateTime.MinValue;
            }

            if (lastPurge.AddMinutes(15) < DateTime.Now)
            {
                foreach (var i in _db.AlertEntries)
                {
                    if (i.endtime < DateTime.Now)
                    {
                        _db.AlertEntries.Remove(i);
                    }

                    
                }
                _db.SaveChanges();
                lastPurge = DateTime.Now;
                return Content("OK");

            }
            else
            {
                throw new Exception("Purge is only permitted once every 15 minutes.");
            }

            return ViewAll();
        }

        [HttpPost]
        public ActionResult GetAlertData()
        {
            AlertDataResponse response = new AlertDataResponse();
            Request.InputStream.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(Request.InputStream).ReadToEnd();
            AlertDataRequest request = JsonConvert.DeserializeObject<AlertDataRequest>(json);
            response.alerts = new List<Alert>();
            response.foundAlert = false;
            foreach (var i in _db.AlertEntries)
            {
                if(request.getAll || request.requestedAlert.Equals(Guid.Parse(i.guid)))
                {
                    response.alerts.Add(fromDatabase(i));
                    response.foundAlert = true;
                }
            }

            return Content(JsonConvert.SerializeObject(response));
        }

        private bool hasAlert(Guid toFind)
        {
            foreach (var i in _db.AlertEntries.ToList())
            {
                if (i.guid.Equals(toFind.ToString()))
                {
                    return true;
                }
            }

            return false;
        }

        private AlertEntry getAlert(Guid toFind)
        {
            foreach (var i in _db.AlertEntries.ToList())
            {
                if (i.guid.Equals(toFind.ToString()))
                {
                    return i;
                }
            }

            return null;
        }

        private Alert fromDatabase(AlertEntry data)
        {
            Alert newAlert = new Alert();
            newAlert.myID = Guid.Parse(data.guid);
            newAlert.myType = (Resources.AlertTypes)data.type;
            newAlert.myLoc = (Resources.LocationShortCode) data.location;
            newAlert.startTime = data.starttime;
            newAlert.endTime = data.endtime;
            newAlert.isIndefinite = data.indefinite;
            newAlert.message = data.message;
            return newAlert;

        }
    }
}