﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MakiNet.Models
{
    public class Resources
    {
        public static string URL = "http://localhost:60281";
        public static string Level0 = "green-bar"; // Watch, Admin
        public static string Level1 = "orange-bar"; // Advisory
        public static string Level2 = "red-bar"; // Warning
        public enum AlertTypes
        {
            /*
             * Begin Standard SAME CODES
             * See https://en.wikipedia.org/wiki/Specific_Area_Message_Encoding for code definitions or look below
             */
            ADR,
            BZW,
            CDW,
            CEM,
            EQW,
            EVI,
            EWW,
            FFA,
            FFS,
            FFW,
            FLA,
            FLS,
            FLW,
            FRW,
            FSW,
            FZW,
            HMW,
            HWA,
            HWW,
            LAE,
            LEW,
            RHW,
            RMT,
            RWT,
            SPS,
            SPW,
            SVA,
            SVR,
            SVS,
            VOW,
            WSA,
            WSW,
            /* Begin Makislavia Extended Set */
            NAT, // NATIONAL EMERGENCY
            WWA, // WINTER WEATER ADVISORY
            WWW, // WINTER WEATHER WATCH
            UNC, // UNCLASSIFIED ALERT
            WA, // WIND ADVISORY
            RFW, // RED FLAG WARNING
            THA, // THUNDERSTORM WATCH
            THW, // THUNDERSTORM WARNING (level 1 only)
            POS, // POWER OUTAGE STATEMENT
            HTA, // HEAT ADVISORY
            HTW, // HEAT WARNING
        };
        public static Dictionary<int, string> AlertStrings = new Dictionary<int, string>()
        {
            {0, "Administrative Message"},
            {1, "Blizzard Warning" },
            {2, "Civil Danger Warning" },
            {3, "Civil Emergency Message" },
            {4, "Earthquake Warning" },
            {5, "Evacuation Immediate" },
            {6, "Extreme Wind Warning" },
            {7, "Flash Flood Watch" },
            {8, "Flash Flood Statement" },
            {9, "Flash Flood Warning" },
            {10, "Flood Watch" },
            {11, "Flood Statement" },
            {12, "Flood Warning" },
            {13, "Fire Warning" },
            {14, "Flash Freeze Warning" },
            {15, "Freeze Warning" },
            {16, "Hazardous Materials Warning" },
            {17, "High Wind Watch" },
            {18, "High Wind Warning" },
            {19, "Local Area Emergency" },
            {20, "Law Enforcement Warning" },
            {21, "Radiological Hazard Warning" },
            {22, "Required Monthly Test" },
            {23, "Required Weekly Test" },
            {24, "Special Weather Statement" },
            {25, "Shelter In-place Warning" },
            {26, "Severe Thunderstorm Watch" },
            {27, "Severe Thunderstorm Warning" },
            {28, "Severe Weather Statement" },
            {29, "Volcano Warning" },
            {30, "Winter Storm Watch" },
            {31, "Winter Storm Warning" },
            /* Begin Makislavia Extended Set */
            {32, "National Emergency" },
            {33, "Winter Weather Advisory" },
            {34, "Winter Weather Watch" },
            {35, "Unclassified Alert" },
            {36, "Wind Advisory" },
            {37, "Red Flag Warning" },
            {38, "Thunderstorm Watch" },
            {39, "Thunderstorm Warning" },
            {40, "Power Outage Statement" },
            {41, "Heat Advisory" },
            {42, "Heat Warning" }
        };

        public static Dictionary<int, string> AlertSeverity = new Dictionary<int, string>()
        {
            {0, Level0 },
            {1, Level2 },
            {2, Level2 },
            {3, Level1 },
            {4, Level2 },
            {5, Level2 },
            {6, Level2 },
            {7, Level0 },
            {8, Level1 },
            {9, Level2 },
            {10, Level0 },
            {11, Level1 },
            {12, Level2 },
            {13, Level2 },
            {14, Level2 },
            {15, Level2 },
            {16, Level2 },
            {17, Level0 },
            {18, Level2 },
            {19, Level2 },
            {20, Level2 },
            {21, Level2 },
            {22, Level0 },
            {23, Level0 },
            {24, Level1 },
            {25, Level2 },
            {26, Level0 },
            {27, Level2 },
            {28, Level2 },
            {29, Level2 },
            {30, Level0 },
            {31, Level2 },
            {32, Level2 },
            {33, Level1 },
            {34, Level0 },
            {35, Level0 },
            {36, Level1 },
            {37, Level2 },
            {38, Level0 },
            {39, Level1 },
            {40, Level1 },
            {41, Level1 },
            {42, Level2 }

        };
        public enum LocationShortCode
        {
            ALL,
            CM,
            SM,
            EM,
            NM
        };
        public static Dictionary<int, string> LocationCodes = new Dictionary<int, string>()
        {
            {0, "All Service Areas"},
            {1, "Central Makislavia"},
            {2, "South Makislavia"},
            {3, "East Makislavia"},
            {4, "North Makislavia"}
        };

        public static Dictionary<int, List<LocationShortCode>> LocationZipMapping = new Dictionary<int, List<LocationShortCode>>()
        {
            {98012, new List<LocationShortCode>(){LocationShortCode.SM,LocationShortCode.CM}},
            {98021, new List<LocationShortCode>(){LocationShortCode.SM}},
            {98208, new List<LocationShortCode>(){LocationShortCode.NM,LocationShortCode.EM}},
            {98296, new List<LocationShortCode>(){LocationShortCode.EM}},
            {98082, new List<LocationShortCode>(){LocationShortCode.CM}}
        };
    }
}