﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MakiNet.Models
{
    public enum Action
    {
        Add,
        Update,
        Remove
    };
    public class AlertActionRequest
    {
        public Guid key { get; set; }
        public Action action { get; set; }
        public Alert alert { get; set; }
    }
}