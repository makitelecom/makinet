﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MakiNet.Models;
using Newtonsoft.Json;
using Action = MakiNet.Models.Action;

namespace MakiNet.Views.Alert
{
    public partial class UpdateAlert : System.Web.UI.Page
    {
        public AlertActionRequest request
        {
            get { return (AlertActionRequest) Session["AlertActionRequest"]; }
            set { Session["AlertActionRequest"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                request = new AlertActionRequest();
                request.alert = new Models.Alert();
                request.action = Action.Update;
                DropDownList1.DataValueField = "Key";
                DropDownList1.DataTextField = "Value";
                DropDownList1.DataSource = Resources.AlertStrings;
                DropDownList1.DataBind();
                DropDownList2.DataValueField = "Key";
                DropDownList2.DataTextField = "Value";
                DropDownList2.DataSource = Resources.LocationCodes;
                DropDownList2.DataBind();
            }
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox1.Checked)
            {
                TextBox2.Text = DateTime.MaxValue.ToString();
            } else
            {
                TextBox2.Text = "";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                request.key = Guid.Parse(TextBox4.Text);
                request.alert.myID = Guid.Parse(TextBox6.Text);
                request.alert.myType = (Resources.AlertTypes) int.Parse(DropDownList1.SelectedValue);
                request.alert.myLoc = (Resources.LocationShortCode) int.Parse(DropDownList2.SelectedValue);
                request.alert.startTime = DateTime.Parse(TextBox1.Text);
                request.alert.endTime = DateTime.Parse(TextBox2.Text);
                request.alert.isIndefinite = CheckBox1.Checked;
                request.alert.message = TextBox3.Text;

                string post = Resources.URL + "/Alert/PerformAction";
                var content = JsonConvert.SerializeObject(request);
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string HtmlResult = wc.UploadString(post, content);
                    TextBox5.Text = HtmlResult;
                }
            }
            catch (Exception ex)
            {
                TextBox5.Text = "An unknown error occurred. Please check your input.";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            AlertDataRequest alertRequest = new AlertDataRequest();
            alertRequest.requestedAlert = Guid.Parse(TextBox6.Text);
            alertRequest.getAll = false;
            string post = Resources.URL + "/Alert/GetAlertData";
            var content = JsonConvert.SerializeObject(alertRequest);
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string HtmlResult = wc.UploadString(post, content);
                AlertDataResponse recvAlert = JsonConvert.DeserializeObject<AlertDataResponse>(HtmlResult);
                if (recvAlert.foundAlert)
                {
                    Models.Alert found = recvAlert.alerts.First();
                    DropDownList1.SelectedValue = ((int)found.myType).ToString();
                    DropDownList2.SelectedValue = ((int)found.myLoc).ToString();
                    TextBox1.Text = found.startTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', 'T');
                    TextBox2.Text = found.endTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(' ', 'T');
                    CheckBox1.Checked = found.isIndefinite;
                    TextBox3.Text = found.message;
                }
            }
        }
    }
}