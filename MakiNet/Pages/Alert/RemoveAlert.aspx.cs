﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MakiNet.Models;
using Newtonsoft.Json;
using Action = MakiNet.Models.Action;

namespace MakiNet.Views.Alert
{
    public partial class RemoveAlert : System.Web.UI.Page
    {
        public AlertActionRequest request
        {
            get { return (AlertActionRequest) Session["AlertActionRequest"]; }
            set { Session["AlertActionRequest"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                request = new AlertActionRequest();
                request.alert = new Models.Alert();
                request.action = Action.Remove;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                request.key = Guid.Parse(TextBox4.Text);
                request.alert.myID = Guid.Parse(TextBox6.Text);

                string post = Resources.URL + "/Alert/PerformAction";
                var content = JsonConvert.SerializeObject(request);
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string HtmlResult = wc.UploadString(post, content);
                    TextBox5.Text = HtmlResult;
                }
            }
            catch (Exception ex)
            {
                TextBox5.Text = "An unknown error occurred. Please check your input.";
            }
        }
    }
}