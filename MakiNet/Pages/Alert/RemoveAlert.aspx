﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RemoveAlert.aspx.cs" Inherits="MakiNet.Views.Alert.RemoveAlert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    </head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Remove Alert</h1>
            GUID:&nbsp;
            <asp:TextBox ID="TextBox6" runat="server" Width="338px"></asp:TextBox>
            <br />
            Operator Key:
            <asp:TextBox ID="TextBox4" runat="server" Width="297px"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit" />
        </div>
        <br />
        Response:<br />
        <asp:TextBox ID="TextBox5" runat="server" Height="140px" ReadOnly="True" TextMode="MultiLine" Width="304px"></asp:TextBox>
    </form>
</body>
</html>
