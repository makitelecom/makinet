﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MakiNet.Models
{
    public class AlertDataRequest
    {
        public Guid requestedAlert { get; set; }
        public bool getAll { get; set; }
    }
}